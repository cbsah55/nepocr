package com.cbs.nepocr.service;

import lombok.SneakyThrows;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.ImageHelper;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class OcrService {
    private ITesseract tesseract;

    public OcrService() {
        this.tesseract = new Tesseract();
        this.tesseract.setDatapath("/usr/share/tessdata/");
        this.tesseract.setLanguage("eng");
    }

    public ResponseEntity<String> getTextFromImage(MultipartFile imageFile,String lang) {
        this.tesseract.setLanguage(lang);
        try {
            BufferedImage bufferedImage = ImageIO.read(imageFile.getInputStream());
            String textFromImage = tesseract.doOCR(bufferedImage);
            return ResponseEntity.ok(textFromImage);
        } catch (IOException | TesseractException e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> getTextFromPdf(MultipartFile pdfFile,String lang) {
        this.tesseract.setLanguage(lang);
        try {

            PDDocument pdDocument = PDDocument.load(pdfFile.getBytes());
            //getting text from pdf if pdf is already a textfile that machine understands
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            String strippedText = pdfTextStripper.getText(pdDocument);
            if (strippedText.trim().isEmpty()) {
                strippedText = getTextFromScannedPdfDocument(pdDocument);
            }
            pdDocument.close();
            return new ResponseEntity<String>(strippedText, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @SneakyThrows({IOException.class, TesseractException.class})
    private String getTextFromScannedPdfDocument(PDDocument pdDocument) {

        PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);
        StringBuilder outText = new StringBuilder();
        for (int page = 0; page < pdDocument.getNumberOfPages(); page++) {
            //converting scanned pdf documents to images
            BufferedImage bufferedImage = pdfRenderer.renderImageWithDPI(page, 300, ImageType.ARGB);
            //Storing each pages as temp file
            File tempFile = File.createTempFile("tempfile_" + page, ".png");
            ImageIO.write(bufferedImage, "png", tempFile);
            //Get buffered image, to convert it into grayscale
            BufferedImage bmi = ImageIO.read(tempFile);
            // tesseract to perform OCR
            String result = tesseract.doOCR(ImageHelper.convertImageToGrayscale(bmi));
            outText.append(result);

        }
        return outText.toString();
    }


}










