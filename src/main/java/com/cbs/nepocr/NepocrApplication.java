package com.cbs.nepocr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class NepocrApplication {

    public static void main(String[] args) {
        SpringApplication.run(NepocrApplication.class, args);
    }

}
