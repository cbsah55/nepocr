package com.cbs.nepocr.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.cbs.nepocr.service.OcrService;

@RestController()
@RequestMapping("/api/")
@CrossOrigin(origins = "http://localhost:8081")
@RequiredArgsConstructor
public class OcrController {

    private final OcrService ocrService;

    @GetMapping("/test")
    public ResponseEntity<String> test(){
        return ResponseEntity.ok("hello world");
    }

    @PostMapping("/uploadFile")
    public ResponseEntity<String> getImage(@RequestParam("file")MultipartFile file,
                                           @RequestParam() String lang){
        if (file.getContentType().equals(MediaType.APPLICATION_PDF_VALUE)){
            return ocrService.getTextFromPdf(file,lang);
        }else {
            return ocrService.getTextFromImage(file,lang);
        }

    }

    @PostMapping("/uploadPdf")
    public ResponseEntity<String> convertPdfToText(
            @RequestParam("file") MultipartFile pdfFile,
            @RequestParam() String lang){
        return ocrService.getTextFromPdf(pdfFile,lang);
    }

}
