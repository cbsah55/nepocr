FROM fabric8/java-alpine-openjdk8-jre


RUN apk update

# Install tesseract library
RUN apk add --no-cache tesseract-ocr


# Dowlnoad last language package
RUN mkdir -p /usr/share/tessdata
#ADD https://github.com/tesseract-ocr/tessdata/raw/master/nep.traineddata /usr/share/tessdata/nep.traineddata
#ADD https://github.com/tesseract-ocr/tessdata/raw/master/eng.traineddata /usr/share/tessdata/eng.traineddata

COPY /tessdata/eng.traineddata /usr/share/tessdata/eng.traineddata
COPY /tessdata/nep.traineddata /usr/share/tessdata/nep.traineddata


# Check the installation status
RUN tesseract --list-langs
RUN tesseract -v

# set the location of the jar
ENV USER_HOME /usr/app
WORKDIR $USER_HOME

#Set the name of the jar
ENV APP_FILE nepocr-0.0.1-test.jar


# copy out jar
COPY build/libs/$APP_FILE $USER_HOME/app.jar
# Open the port
EXPOSE 8080

#CMD ["java","-jar","app.jar"]

# Launch the Spring Boot application
ENTRYPOINT [ "sh", "-c", "java -jar app.jar" ]