This application converts image to text.

To run this application install docker in your system and follow the steps:
   
  ` run ./gradlew clean build` navigate to root directory and from terminal execute

1). Go to the root directory of the project and run command
     `docker build -t cbsah55/nep-ocr (Can be your own name)` 

2). Check if your image has been created or not:
    ```
    docker images 
    (You will see docker image has been created with the name cbsah55/nep-ocr)
    ```

3). Run the docker image by following command:
     `docker run -t -i -p 8080:8080 cbsah55/nep-ocr`

4). You will see applicatio running (Do not close the terminal)
5). User postman to check if the endpoint is working or not
    `localhost:8080/api/test`

6). To get text from image the endpoint is following :
    ```
    localhost:8080/api/uploadImage or localhost:8080/api/uploadPdf
    params - {
        file - image_file or pdf_file
        "lang" - "eng" or "nep"
    }
    ```


